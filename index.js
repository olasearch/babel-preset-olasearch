module.exports = {
  presets: [
    require('babel-preset-env'),
    require('babel-preset-react'),
    require('babel-preset-stage-0')
  ],
  plugins: [
    require("babel-plugin-transform-runtime").default,
    require("babel-plugin-ramda").default,
    require("babel-plugin-transform-es3-member-expression-literals"),
    require("babel-plugin-transform-es3-property-literals"),
    require("babel-plugin-transform-object-set-prototype-of-to-assign"),
    require("babel-plugin-transform-react-remove-prop-types").default,
    require("babel-plugin-transform-react-constant-elements")
  ]
}