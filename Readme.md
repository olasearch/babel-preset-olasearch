# babel-preset-olasearch

> A babel preset for Ola Search core and components

## Install

```sh
$ npm install --save-dev babel-preset-olasearch
```

## Usage

### Via `.babelrc` (Recommended)

**.babelrc**

```json
{
  "presets": ["olasearch"]
}
```

### Via CLI

```sh
$ babel script.js --presets olasearch
```

### Via Node API

```javascript
require("babel-core").transform("code", {
  presets: ["olasearch"]
});
```
